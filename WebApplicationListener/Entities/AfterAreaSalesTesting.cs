﻿using System;
using System.Collections.Generic;

namespace WebApplicationListener.Entities
{
    public partial class AfterAreaSalesTesting
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
    }
}
