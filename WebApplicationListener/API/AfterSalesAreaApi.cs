﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebApplicationListener.Entities;

namespace WebApplicationListener.API
{

    [Route("api/v1/after-sales-area")]
    [ApiController]
    public class AfterSalesAreaApi : ControllerBase
    {

        private readonly TestingDatabaseContext DB;
        private readonly IHttpClientFactory ClientFactory;

        public AfterSalesAreaApi(TestingDatabaseContext testing, IHttpClientFactory ihcf)
        {
            this.DB = testing;
            this.ClientFactory = ihcf;
        }

        [HttpPost]
        public async Task<IActionResult> PostAfterSalesArea([FromBody]recieveAfterSalesArea listArea)
        {

            if(ModelState.IsValid == false)
            {
                return BadRequest();
            }

            var newAfterSalesArea = new AfterAreaSalesTesting
            {
                Code = listArea.Code,
                Name = listArea.Name,
                State = listArea.State
            };

            this.DB.AddRange(newAfterSalesArea);
            await this.DB.SaveChangesAsync();

            return Ok();
        }
    }

    public class recieveAfterSalesArea
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string State { get; set; }
    }
}
